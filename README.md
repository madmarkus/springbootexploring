# Learn project to get familiar with Spring Boot


You can build and run this sample using Gradle Wrapper:

```
$ gradlew run
```

If you want to run the application outside of Gradle, then first build the JARs
and then use the `java` command:

```
$ gradle build
$ java -jar build/libs/*.jar
```

Then access the app via a browser (or curl) on http://localhost:8080.
