package sample;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Weather {

    @Id
    @GeneratedValue
    public String id;

    @Column
    public String temperature;

    @Column
    public String humidity;

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String inHumidity) {
        humidity = inHumidity;
    }

    public String getId() {
        return id;
    }

    public void setId(String inId) {
        id = inId;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String inTemperature) {
        temperature = inTemperature;
    }
}
